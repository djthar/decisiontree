#include <iostream>
#include <cstdint>
#include "DecisionTreeBuilder.h"
#include "rapidjson/document.h"

using namespace rapidjson;

template <typename T, typename FType, typename D>
std::vector<T> toStdVector(const D& input, FType f) {
    const auto temp = input.GetArray();
    std::vector<T> output;
    for(const auto &s : input.GetArray()) {
        output.emplace_back((s.*f)());
    }
    return output;
}

int main() {
    std::string tree{"{\n"
                     "  \"features\": [\"averagePacketSize\", \"totalLength\"],\n"
                     "  \"decisionClasses\": [\"ClassA\", \"ClassB\", \"ClassC\"],\n"
                     "  \"featuresType\": \"int32_t\",\n"
                     "  \"nodes\": [{\n"
                     "    \"id\": 1,\n"
                     "    \"type\": \"SplitNode\",\n"
                     "    \"feature\": \"averagePacketSize\",\n"
                     "    \"operation\": \">\",\n"
                     "    \"threshold\": \"3\",\n"
                     "    \"parentId\": 0,\n"
                     "    \"parentBranch\": \"false\"\n"
                     "  },{\n"
                     "    \"id\": 2,\n"
                     "    \"type\": \"SplitNode\",\n"
                     "    \"feature\": \"TotalLength\",\n"
                     "    \"operation\": \">\",\n"
                     "    \"threshold\": \"77\",\n"
                     "    \"parentId\": 1,\n"
                     "    \"parentBranch\": \"true\"\n"
                     "  },{\n"
                     "    \"id\": 3,\n"
                     "    \"type\": \"LeafNode\",\n"
                     "    \"decisionClass\": \"ClassA\",\n"
                     "    \"parentId\": 1,\n"
                     "    \"parentBranch\": \"false\"\n"
                     "  },{\n"
                     "    \"id\": 4,\n"
                     "    \"type\": \"LeafNode\",\n"
                     "    \"decisionClass\": \"ClassB\",\n"
                     "    \"parentId\": 2,\n"
                     "    \"parentBranch\": \"true\"\n"
                     "  },{\n"
                     "    \"id\": 5,\n"
                     "    \"type\": \"LeafNode\",\n"
                     "    \"decisionClass\": \"ClassC\",\n"
                     "    \"parentId\": 2,\n"
                     "    \"parentBranch\": \"false\"\n"
                     "  }]\n"
                     "}"};

    Document document;
    document.Parse(tree.c_str());

    const auto features = toStdVector<std::string>(document["features"], &GenericValue<rapidjson::UTF8<char>>::GetString);
    const auto decisionClasses = toStdVector<std::string>(document["decisionClasses"], &GenericValue<rapidjson::UTF8<char>>::GetString);

    DecisionTreeBuilder<int32_t> decisionTreeBuilder(features, decisionClasses);

    for (const auto &n : document["nodes"].GetArray()) {
        decisionTreeBuilder.addNodeToDecisionTree(n);
    }
    auto dt = decisionTreeBuilder.build();

    std::cout << dt.evaluate({1, 3}) << " " << dt.evaluateToString({1, 3}) << std::endl;

    DecisionTreeBuilder<int32_t> decisionTreeBuilder2({"averagePacketSize", "totalLength"}, {"ClassA", "ClassB", "ClassC"});
    decisionTreeBuilder2.addNodeToDecisionTree({"1,averagePacketSize,>,3,_,0,false"});  // root node
    decisionTreeBuilder2.addNodeToDecisionTree({"2,totalLength,>,77',_,1,true"});  // a split node
    decisionTreeBuilder2.addNodeToDecisionTree({"3,_,_,_,ClassA,1,false"});  // leaf
    decisionTreeBuilder2.addNodeToDecisionTree({"4,_,_,_,ClassB,2,true"});  // leaf
    decisionTreeBuilder2.addNodeToDecisionTree({"5,_,_,_,ClassC,2,false"});  // leaf
    auto dt2 = decisionTreeBuilder2.build();

    std::cout << dt2.evaluate({1, 3}) << " " << dt2.evaluateToString({1, 3}) << std::endl;

    return 0;
}