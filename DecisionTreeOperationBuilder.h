#ifndef DECISIONTREEEXAMPLE_DECISIONTREEOPERATIONBUILDER_H
#define DECISIONTREEEXAMPLE_DECISIONTREEOPERATIONBUILDER_H


#include <functional>
#include <unordered_map>

class DecisionTreeOperationBuilder {
public:
    template <typename T>
    static std::function<bool(T, T)> getOperation(std::string operation) {
        static std::unordered_map<std::string, std::function<bool(T, T)> > operations({
            {">", [](T a, T b){ return a > b; }},
            {"<", [](T a, T b){ return a < b; }},
            {">=", [](T a, T b){ return a >= b; }},
            {"<=", [](T a, T b){ return a <= b; }},
            {"==", [](T a, T b){ return a == b; }},
            {"!=", [](T a, T b){ return a != b; }},
        });
        return operations[operation];
    }
};


#endif //DECISIONTREEEXAMPLE_DECISIONTREEOPERATIONBUILDER_H
