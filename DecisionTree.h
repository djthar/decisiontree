#ifndef DECISIONTREEEXAMPLE_DECISIONTREE_H
#define DECISIONTREEEXAMPLE_DECISIONTREE_H


#include <algorithm>
#include <vector>
#include <memory>
#include <string>
#include <unordered_map>
#include "Node.h"

template <typename T>
class DecisionTree {
public:
    void setFeatures(const std::vector<std::string> &features) {
        features_ = features;
    }

    const std::vector<std::string>& getNeededFeatures() const {
        return features_;
    }

    long getFeaturePosition(const std::string &feature) const {
        return std::distance(features_.begin(), std::find(features_.begin(), features_.end(), feature));
    }

    void setDecisionClasses(const std::vector<std::string> &decisionClasses) {
        decisionClasses_ = decisionClasses;
    }

    long getDecisionClassPosition(const std::string &decisionClass) const {
        return std::distance(decisionClasses_.begin(), std::find(decisionClasses_.begin(), decisionClasses_.end(), decisionClass));
    }

    std::string getDecisionClassName(long decisionClassId) const {
        return decisionClasses_[decisionClassId];
    }

    long evaluate(const std::vector<T> &featuresValues) {
        // TODO: assert or exception or better error handling
        if (featuresValues.size() != features_.size()) {
            // ERROR
            return 0;
        }
        return nodes_[1]->evaluate(featuresValues);
    }

    std::string evaluateToString(const std::vector<T> &featuresValues) {
        return decisionClasses_[evaluate(featuresValues)];
    }

    void addNode(unsigned int id, std::unique_ptr<BNode<T>> node) {
        if (nodes_.find(id) != nodes_.end()) {
            // TODO: ERROR
            return;
        }
        nodes_.insert(std::make_pair(id, std::move(node)));
    }

    BNode<T>* getNode(unsigned int id) {
        if (nodes_.find(id) != nodes_.end()) {
            return nodes_[id].get();
        } else {
            return nullptr;
        }
    }


private:
    std::unordered_map<unsigned int, std::unique_ptr<BNode<T> > > nodes_;
    std::vector<std::string> features_;
    std::vector<std::string> decisionClasses_;
};


#endif //DECISIONTREEEXAMPLE_DECISIONTREE_H
