#ifndef DECISIONTREEEXAMPLE_DECISIONTREEBUILDER_H
#define DECISIONTREEEXAMPLE_DECISIONTREEBUILDER_H


#include <string>
#include <sstream>
#include <vector>
#include "DecisionTree.h"
#include "Node.h"
#include "DecisionTreeOperationBuilder.h"
#include "rapidjson/rapidjson.h"
#include "rapidjson/document.h"

using namespace rapidjson;

template <typename T>
class NodeRepresentation {
public:
    NodeRepresentation(std::string nodeStringRepresentation) {
        auto tokens = split(nodeStringRepresentation, ',');
        //"id,featureName,operation,threshold,class,parentId,branchSide"
        // 0  1           2         3         4     5        6
        nodeId = static_cast<unsigned int>(std::stoul(tokens[0]));
        parentId = static_cast<unsigned int>(std::stoul(tokens[5]));
        isTrueBranchSide = tokens[6] == "true";
        type = tokens[3] != "_" ? "SplitNode" : "LeafNode";
        isLeaf = type == "LeafNode";
        if (isLeaf) {
            decisionClassName = tokens[4];
        } else {
            featureName = tokens[1];
            operationString = tokens[2];
            featureThreshold = convert(tokens[3]);
        }
    }

    NodeRepresentation(const GenericValue<rapidjson::UTF8<char>>& nodeJson) {
        nodeId = nodeJson["id"].GetUint();
        parentId = nodeJson["parentId"].GetUint();
        isTrueBranchSide = std::string(nodeJson["parentBranch"].GetString()) == "true";
        type = nodeJson["type"].GetString();
        isLeaf = type == "LeafNode";
        if (isLeaf) {
            decisionClassName = nodeJson["decisionClass"].GetString();
        } else {
            featureName = nodeJson["feature"].GetString();
            operationString = nodeJson["operation"].GetString();
            featureThreshold = convert(nodeJson["threshold"].GetString());
        }
    }

    unsigned int nodeId{};
    std::string featureName;
    std::string operationString;
    T featureThreshold{};
    std::string decisionClassName;
    unsigned int parentId{};
    bool isTrueBranchSide{};
    std::string type{};
    bool isLeaf{};

private:
    std::vector<std::string> split(const std::string &s, char delim) {
        std::stringstream ss(s);
        std::string item;
        std::vector<std::string> elems;
        while (std::getline(ss, item, delim)) {
            elems.push_back(std::move(item));
        }
        return elems;
    }

    T convert(const std::string& value) {
        std::istringstream is(value);
        T output;
        is >> output;
        return output;
    }

};

template <typename T>
class DecisionTreeBuilder {
public:

    DecisionTreeBuilder(const std::vector<std::string> &features, const std::vector<std::string> &decisionClasses) {
        dt.setFeatures(features);
        dt.setDecisionClasses(decisionClasses);
    }

    void addNodeToDecisionTree(const NodeRepresentation<T> &nodeRepresentation) {
        BNode<T> * parentNode = dt.getNode(nodeRepresentation.parentId);
        auto node = nodeRepresentation.isLeaf ?
                std::unique_ptr<BNode<T>>(new LeafNode<T>(
                        dt.getDecisionClassPosition(nodeRepresentation.decisionClassName), parentNode)) :
                std::unique_ptr<BNode<T>>(new SplitNode<T>(
                        dt.getFeaturePosition(nodeRepresentation.featureName), nodeRepresentation.featureThreshold,
                        DecisionTreeOperationBuilder::getOperation<T>(nodeRepresentation.operationString), parentNode));
        if (parentNode) {
            if (nodeRepresentation.isTrueBranchSide) {
                parentNode->setTrueBranchNode(node.get());
            } else {
                parentNode->setFalseBranchNode(node.get());
            }
        }
        dt.addNode(nodeRepresentation.nodeId, std::move(node));
    }

    DecisionTree<T> build() {
            DecisionTree<T> output;
            std::swap(dt, output);
            return output;
    }

private:
    DecisionTree<T> dt;
};


#endif //DECISIONTREEEXAMPLE_DECISIONTREEBUILDER_H
