#ifndef DECISIONTREEEXAMPLE_NODE_H
#define DECISIONTREEEXAMPLE_NODE_H


#include <functional>
#include <vector>

template <typename T>
class BNode {
public:
    BNode(BNode* parent) : parentNode_(parent) {}
    virtual long evaluate(const std::vector<T> &featuresValues) const = 0;
    virtual void setTrueBranchNode(BNode *node) = 0;
    virtual void setFalseBranchNode(BNode *node) = 0;
    void setParentNode(BNode * parentNode) {
        parentNode_ = parentNode;
    }
    BNode * getParentNode() {
        return parentNode_;
    }
    virtual ~BNode() = default;

protected:
    BNode* parentNode_{nullptr};
};

template <typename T>
class SplitNode : public BNode<T> {
public:
    SplitNode(long featurePosition, T featureThreshold,
            std::function<bool(T, T)>operation, BNode<T>* parent) : BNode<T>(parent), featurePosition_(featurePosition),
            featureThreshold_(featureThreshold), operation_(operation) {
    }
    virtual long evaluate(const std::vector<T> &featuresValues) const {
        BNode<T> * evaluationNode = operation_(featuresValues[featurePosition_], featureThreshold_) ? trueNode_ : falseNode_;
        return evaluationNode->evaluate(featuresValues);
    }
    virtual void setTrueBranchNode(BNode<T> * node) {
        trueNode_ = node;
        node->setParentNode(this);
    }
    virtual void setFalseBranchNode(BNode<T> * node) {
        falseNode_ = node;
        node->setParentNode(this);
    }

private:
    const long featurePosition_;
    const T featureThreshold_;
    const std::function<bool(T, T)> operation_;
    BNode<T>* trueNode_;
    BNode<T>* falseNode_;
};

template <typename T>
class LeafNode : public BNode<T> {
public:
    LeafNode(long decisionClass, BNode<T>* parent) : BNode<T>(parent), decisionClass_(decisionClass) {}
    virtual long evaluate(const std::vector<T> &featuresValues) const {
        return decisionClass_;
    }
    virtual void setTrueBranchNode(BNode<T> * node) { /*ERROR*/ }
    virtual void setFalseBranchNode(BNode<T> * node) { /*ERROR*/ }

private:
    const long decisionClass_;
};


#endif //DECISIONTREEEXAMPLE_NODE_H
